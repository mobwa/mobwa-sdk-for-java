# AccountsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createAccount**](AccountsApi.md#createAccount) | **POST** /accounts | Create an account
[**getAccount**](AccountsApi.md#getAccount) | **GET** /accounts/{accountId} | Get account information
[**listAccount**](AccountsApi.md#listAccount) | **GET** /accounts | List all accounts
[**rechargeAccount**](AccountsApi.md#rechargeAccount) | **POST** /accounts/{accountId}/recharge | Recharge the account
[**updateAccount**](AccountsApi.md#updateAccount) | **PUT** /accounts/{accountId} | Update account information
[**withdrawMoney**](AccountsApi.md#withdrawMoney) | **POST** /accounts/{accountId}/withdraw | Withdraw money from the account


<a name="createAccount"></a>
# **createAccount**
> List&lt;Transfer&gt; createAccount(createAccountRequest)

Create an account

### Example
```java
// Import classes:
import io.gitlab.mobwa.client.ApiClient;
import io.gitlab.mobwa.client.ApiException;
import io.gitlab.mobwa.client.Configuration;
import io.gitlab.mobwa.client.auth.*;
import io.gitlab.mobwa.client.models.*;
import io.gitlab.mobwa.client.api.AccountsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    AccountsApi apiInstance = new AccountsApi(defaultClient);
    CreateAccountRequest createAccountRequest = new CreateAccountRequest(); // CreateAccountRequest | 
    try {
      List<Transfer> result = apiInstance.createAccount(createAccountRequest);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling AccountsApi#createAccount");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createAccountRequest** | [**CreateAccountRequest**](CreateAccountRequest.md)|  | [optional]

### Return type

[**List&lt;Transfer&gt;**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | A paged array of pets |  -  |
**0** | unexpected error |  -  |

<a name="getAccount"></a>
# **getAccount**
> Account getAccount(accountId)

Get account information

### Example
```java
// Import classes:
import io.gitlab.mobwa.client.ApiClient;
import io.gitlab.mobwa.client.ApiException;
import io.gitlab.mobwa.client.Configuration;
import io.gitlab.mobwa.client.auth.*;
import io.gitlab.mobwa.client.models.*;
import io.gitlab.mobwa.client.api.AccountsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    AccountsApi apiInstance = new AccountsApi(defaultClient);
    String accountId = "accountId_example"; // String | The id of the account
    try {
      Account result = apiInstance.getAccount(accountId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling AccountsApi#getAccount");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **String**| The id of the account |

### Return type

[**Account**](Account.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Expected response to a valid request |  -  |
**0** | unexpected error |  -  |

<a name="listAccount"></a>
# **listAccount**
> List&lt;Account&gt; listAccount()

List all accounts

### Example
```java
// Import classes:
import io.gitlab.mobwa.client.ApiClient;
import io.gitlab.mobwa.client.ApiException;
import io.gitlab.mobwa.client.Configuration;
import io.gitlab.mobwa.client.auth.*;
import io.gitlab.mobwa.client.models.*;
import io.gitlab.mobwa.client.api.AccountsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    AccountsApi apiInstance = new AccountsApi(defaultClient);
    try {
      List<Account> result = apiInstance.listAccount();
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling AccountsApi#listAccount");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List&lt;Account&gt;**](Account.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | A paged array of pets |  -  |
**0** | unexpected error |  -  |

<a name="rechargeAccount"></a>
# **rechargeAccount**
> Account rechargeAccount(accountId, rechargeAccountRequest)

Recharge the account

### Example
```java
// Import classes:
import io.gitlab.mobwa.client.ApiClient;
import io.gitlab.mobwa.client.ApiException;
import io.gitlab.mobwa.client.Configuration;
import io.gitlab.mobwa.client.auth.*;
import io.gitlab.mobwa.client.models.*;
import io.gitlab.mobwa.client.api.AccountsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    AccountsApi apiInstance = new AccountsApi(defaultClient);
    String accountId = "accountId_example"; // String | The id of the account
    RechargeAccountRequest rechargeAccountRequest = new RechargeAccountRequest(); // RechargeAccountRequest | 
    try {
      Account result = apiInstance.rechargeAccount(accountId, rechargeAccountRequest);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling AccountsApi#rechargeAccount");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **String**| The id of the account |
 **rechargeAccountRequest** | [**RechargeAccountRequest**](RechargeAccountRequest.md)|  | [optional]

### Return type

[**Account**](Account.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Expected response to a valid request |  -  |
**0** | unexpected error |  -  |

<a name="updateAccount"></a>
# **updateAccount**
> Account updateAccount(accountId, updateAccountRequest)

Update account information

### Example
```java
// Import classes:
import io.gitlab.mobwa.client.ApiClient;
import io.gitlab.mobwa.client.ApiException;
import io.gitlab.mobwa.client.Configuration;
import io.gitlab.mobwa.client.auth.*;
import io.gitlab.mobwa.client.models.*;
import io.gitlab.mobwa.client.api.AccountsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    AccountsApi apiInstance = new AccountsApi(defaultClient);
    String accountId = "accountId_example"; // String | The id of the account
    UpdateAccountRequest updateAccountRequest = new UpdateAccountRequest(); // UpdateAccountRequest | 
    try {
      Account result = apiInstance.updateAccount(accountId, updateAccountRequest);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling AccountsApi#updateAccount");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **String**| The id of the account |
 **updateAccountRequest** | [**UpdateAccountRequest**](UpdateAccountRequest.md)|  | [optional]

### Return type

[**Account**](Account.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Expected response to a valid request |  -  |
**0** | unexpected error |  -  |

<a name="withdrawMoney"></a>
# **withdrawMoney**
> Account withdrawMoney(accountId, body)

Withdraw money from the account

### Example
```java
// Import classes:
import io.gitlab.mobwa.client.ApiClient;
import io.gitlab.mobwa.client.ApiException;
import io.gitlab.mobwa.client.Configuration;
import io.gitlab.mobwa.client.auth.*;
import io.gitlab.mobwa.client.models.*;
import io.gitlab.mobwa.client.api.AccountsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    AccountsApi apiInstance = new AccountsApi(defaultClient);
    String accountId = "accountId_example"; // String | The id of the account
    RechargeAccountRequest body = new RechargeAccountRequest(); // RechargeAccountRequest | 
    try {
      Account result = apiInstance.withdrawMoney(accountId, body);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling AccountsApi#withdrawMoney");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **String**| The id of the account |
 **body** | **RechargeAccountRequest**|  | [optional]

### Return type

[**Account**](Account.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Expected response to a valid request |  -  |
**0** | unexpected error |  -  |

