

# CreateTransferRequest


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**message** | **String** |  |  [optional]
**amount** | **Integer** |  |  [optional]
**currency** | [**CurrencyEnum**](#CurrencyEnum) |  |  [optional]
**autoComplete** | **Boolean** |  |  [optional]
**destination** | **String** |  |  [optional]



## Enum: CurrencyEnum

Name | Value
---- | -----
SGD | &quot;SGD&quot;
USD | &quot;USD&quot;



