

# SignUpResponse


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **String** |  |  [optional]
**email** | **String** |  |  [optional]
**role** | [**RoleEnum**](#RoleEnum) |  |  [optional]
**accounts** | [**List&lt;Account&gt;**](Account.md) |  |  [optional]



## Enum: RoleEnum

Name | Value
---- | -----
CUSTOMER | &quot;customer&quot;
ADMIN | &quot;admin&quot;



