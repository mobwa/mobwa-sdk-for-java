

# Transfer


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  |  [optional]
**message** | **String** |  |  [optional]
**amount** | **Integer** |  |  [optional]
**currency** | [**CurrencyEnum**](#CurrencyEnum) |  |  [optional]
**createdAt** | **String** |  |  [optional]
**updatedAt** | **String** |  |  [optional]
**status** | [**StatusEnum**](#StatusEnum) |  |  [optional]
**autoComplete** | **Boolean** |  |  [optional]
**source** | [**TransferSource**](TransferSource.md) |  |  [optional]
**destination** | [**TransferSource**](TransferSource.md) |  |  [optional]



## Enum: CurrencyEnum

Name | Value
---- | -----
SGD | &quot;SGD&quot;
USD | &quot;USD&quot;



## Enum: StatusEnum

Name | Value
---- | -----
PENDING | &quot;PENDING&quot;
SUCCEEDED | &quot;SUCCEEDED&quot;
FAILED | &quot;FAILED&quot;



