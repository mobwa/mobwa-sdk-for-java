

# Account


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**createdAt** | **String** |  |  [optional]
**currency** | [**CurrencyEnum**](#CurrencyEnum) |  |  [optional]
**updatedAt** | **String** |  |  [optional]
**balance** | **Integer** |  |  [optional]



## Enum: CurrencyEnum

Name | Value
---- | -----
USD | &quot;USD&quot;
SGD | &quot;SGD&quot;



