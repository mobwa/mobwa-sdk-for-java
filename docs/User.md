

# User


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **String** |  |  [optional]
**email** | **String** |  |  [optional]
**role** | [**RoleEnum**](#RoleEnum) |  |  [optional]



## Enum: RoleEnum

Name | Value
---- | -----
CUSTOMER | &quot;CUSTOMER&quot;
ADMIN | &quot;ADMIN&quot;



