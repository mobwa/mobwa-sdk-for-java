# TransfersApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**completeTransfer**](TransfersApi.md#completeTransfer) | **POST** /accounts/{accountId}/transfers/{transferId}/complete | Complete a transfer
[**createTransfer**](TransfersApi.md#createTransfer) | **POST** /accounts/{accountId}/transfers | Create a transfer
[**deleteTransfer**](TransfersApi.md#deleteTransfer) | **DELETE** /accounts/{accountId}/transfers/{transferId} | Delete a transfer
[**getTransfer**](TransfersApi.md#getTransfer) | **GET** /accounts/{accountId}/transfers/{transferId} | Retrieve information for a specific transfer
[**listTransfers**](TransfersApi.md#listTransfers) | **GET** /accounts/{accountId}/transfers | List all transfers related to the account
[**updateTransfer**](TransfersApi.md#updateTransfer) | **PUT** /accounts/{accountId}/transfers/{transferId} | Change transfer information


<a name="completeTransfer"></a>
# **completeTransfer**
> Transfer completeTransfer(accountId, transferId)

Complete a transfer

### Example
```java
// Import classes:
import io.gitlab.mobwa.client.ApiClient;
import io.gitlab.mobwa.client.ApiException;
import io.gitlab.mobwa.client.Configuration;
import io.gitlab.mobwa.client.auth.*;
import io.gitlab.mobwa.client.models.*;
import io.gitlab.mobwa.client.api.TransfersApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    TransfersApi apiInstance = new TransfersApi(defaultClient);
    String accountId = "accountId_example"; // String | The id of the account
    String transferId = "transferId_example"; // String | The id of the transfer to operate on
    try {
      Transfer result = apiInstance.completeTransfer(accountId, transferId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling TransfersApi#completeTransfer");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **String**| The id of the account |
 **transferId** | **String**| The id of the transfer to operate on |

### Return type

[**Transfer**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Expected response to a valid request |  -  |
**0** | unexpected error |  -  |

<a name="createTransfer"></a>
# **createTransfer**
> Transfer createTransfer(accountId, createTransferRequest)

Create a transfer

### Example
```java
// Import classes:
import io.gitlab.mobwa.client.ApiClient;
import io.gitlab.mobwa.client.ApiException;
import io.gitlab.mobwa.client.Configuration;
import io.gitlab.mobwa.client.auth.*;
import io.gitlab.mobwa.client.models.*;
import io.gitlab.mobwa.client.api.TransfersApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    TransfersApi apiInstance = new TransfersApi(defaultClient);
    String accountId = "accountId_example"; // String | The id of the account
    CreateTransferRequest createTransferRequest = new CreateTransferRequest(); // CreateTransferRequest | 
    try {
      Transfer result = apiInstance.createTransfer(accountId, createTransferRequest);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling TransfersApi#createTransfer");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **String**| The id of the account |
 **createTransferRequest** | [**CreateTransferRequest**](CreateTransferRequest.md)|  | [optional]

### Return type

[**Transfer**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Transfer successfully created |  -  |
**0** | unexpected error |  -  |

<a name="deleteTransfer"></a>
# **deleteTransfer**
> Transfer deleteTransfer(accountId, transferId)

Delete a transfer

### Example
```java
// Import classes:
import io.gitlab.mobwa.client.ApiClient;
import io.gitlab.mobwa.client.ApiException;
import io.gitlab.mobwa.client.Configuration;
import io.gitlab.mobwa.client.auth.*;
import io.gitlab.mobwa.client.models.*;
import io.gitlab.mobwa.client.api.TransfersApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    TransfersApi apiInstance = new TransfersApi(defaultClient);
    String accountId = "accountId_example"; // String | The id of the account
    String transferId = "transferId_example"; // String | The id of the transfers to operate on
    try {
      Transfer result = apiInstance.deleteTransfer(accountId, transferId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling TransfersApi#deleteTransfer");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **String**| The id of the account |
 **transferId** | **String**| The id of the transfers to operate on |

### Return type

[**Transfer**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Null response |  -  |
**0** | unexpected error |  -  |

<a name="getTransfer"></a>
# **getTransfer**
> Transfer getTransfer(accountId, transferId)

Retrieve information for a specific transfer

### Example
```java
// Import classes:
import io.gitlab.mobwa.client.ApiClient;
import io.gitlab.mobwa.client.ApiException;
import io.gitlab.mobwa.client.Configuration;
import io.gitlab.mobwa.client.auth.*;
import io.gitlab.mobwa.client.models.*;
import io.gitlab.mobwa.client.api.TransfersApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    TransfersApi apiInstance = new TransfersApi(defaultClient);
    String accountId = "accountId_example"; // String | The id of the account
    String transferId = "transferId_example"; // String | The id of the transfers to operate on
    try {
      Transfer result = apiInstance.getTransfer(accountId, transferId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling TransfersApi#getTransfer");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **String**| The id of the account |
 **transferId** | **String**| The id of the transfers to operate on |

### Return type

[**Transfer**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Expected response to a valid request |  -  |
**0** | unexpected error |  -  |

<a name="listTransfers"></a>
# **listTransfers**
> List&lt;Transfer&gt; listTransfers(accountId)

List all transfers related to the account

### Example
```java
// Import classes:
import io.gitlab.mobwa.client.ApiClient;
import io.gitlab.mobwa.client.ApiException;
import io.gitlab.mobwa.client.Configuration;
import io.gitlab.mobwa.client.auth.*;
import io.gitlab.mobwa.client.models.*;
import io.gitlab.mobwa.client.api.TransfersApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    TransfersApi apiInstance = new TransfersApi(defaultClient);
    String accountId = "accountId_example"; // String | The id of the account
    try {
      List<Transfer> result = apiInstance.listTransfers(accountId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling TransfersApi#listTransfers");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **String**| The id of the account |

### Return type

[**List&lt;Transfer&gt;**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | A list of transfers related to the the account |  -  |
**0** | unexpected error |  -  |

<a name="updateTransfer"></a>
# **updateTransfer**
> Transfer updateTransfer(accountId, transferId, updateTransferRequest)

Change transfer information

### Example
```java
// Import classes:
import io.gitlab.mobwa.client.ApiClient;
import io.gitlab.mobwa.client.ApiException;
import io.gitlab.mobwa.client.Configuration;
import io.gitlab.mobwa.client.auth.*;
import io.gitlab.mobwa.client.models.*;
import io.gitlab.mobwa.client.api.TransfersApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    TransfersApi apiInstance = new TransfersApi(defaultClient);
    String accountId = "accountId_example"; // String | The id of the account
    String transferId = "transferId_example"; // String | The id of the transfers to operate on
    UpdateTransferRequest updateTransferRequest = new UpdateTransferRequest(); // UpdateTransferRequest | 
    try {
      Transfer result = apiInstance.updateTransfer(accountId, transferId, updateTransferRequest);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling TransfersApi#updateTransfer");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **String**| The id of the account |
 **transferId** | **String**| The id of the transfers to operate on |
 **updateTransferRequest** | [**UpdateTransferRequest**](UpdateTransferRequest.md)|  | [optional]

### Return type

[**Transfer**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Null response |  -  |
**0** | unexpected error |  -  |

