# SignupApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**signup**](SignupApi.md#signup) | **POST** /signup | Signup


<a name="signup"></a>
# **signup**
> SignUpResponse signup(signUpRequest)

Signup

### Example
```java
// Import classes:
import io.gitlab.mobwa.client.ApiClient;
import io.gitlab.mobwa.client.ApiException;
import io.gitlab.mobwa.client.Configuration;
import io.gitlab.mobwa.client.models.*;
import io.gitlab.mobwa.client.api.SignupApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");

    SignupApi apiInstance = new SignupApi(defaultClient);
    SignUpRequest signUpRequest = new SignUpRequest(); // SignUpRequest | 
    try {
      SignUpResponse result = apiInstance.signup(signUpRequest);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling SignupApi#signup");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **signUpRequest** | [**SignUpRequest**](SignUpRequest.md)|  | [optional]

### Return type

[**SignUpResponse**](SignUpResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Information related to the enrolled user |  -  |
**0** | unexpected error |  -  |

